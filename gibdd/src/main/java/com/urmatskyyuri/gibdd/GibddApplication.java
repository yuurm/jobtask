package com.urmatskyyuri.gibdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GibddApplication {

    public static void main(String[] args) {
        SpringApplication.run(GibddApplication.class, args);
    }

}
