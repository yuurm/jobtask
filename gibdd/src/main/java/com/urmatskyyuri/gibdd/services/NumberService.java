package com.urmatskyyuri.gibdd.services;

import com.urmatskyyuri.gibdd.models.CarsNumber;

public interface NumberService {
    CarsNumber randomNumber();
    CarsNumber nextNumber(CarsNumber carsNumber);
}
