package com.urmatskyyuri.gibdd.services.impl;

import com.urmatskyyuri.gibdd.models.CarsNumber;
import com.urmatskyyuri.gibdd.repositories.NumberRepository;
import com.urmatskyyuri.gibdd.services.NumberService;
import com.urmatskyyuri.gibdd.utils.LetterUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Random;

public class NumberServiceImpl implements NumberService {

    @Autowired
    private NumberRepository numberRepository;


    @Override
    public CarsNumber randomNumber() {
        CarsNumber carsNumber = CarsNumber
                .builder()
                .first(LetterUtil.random())
                .number(new Random().nextInt(999))
                .second(LetterUtil.random())
                .third(LetterUtil.random())
                .build();
        return carsNumber;
    }

    @Override
    public CarsNumber nextNumber(CarsNumber carsNumber) {
        if (carsNumber.getNumber() == 999) {
            carsNumber.setNumber(0);
            Character previous = carsNumber.getThird();
            carsNumber.setThird(LetterUtil.next(previous));
            if (previous > carsNumber.getThird()) {
                previous = carsNumber.getSecond();
                carsNumber.setSecond(LetterUtil.next(previous));
                if (previous > carsNumber.getSecond()) {
                    carsNumber.setFirst(LetterUtil.next(carsNumber.getFirst()));
                }
            }
        } else {
            carsNumber.setNumber(carsNumber.getNumber() + 1);
        }
        carsNumber.setId(carsNumber.getId() + 1);
        numberRepository.save(carsNumber);
        return carsNumber;
    }
}
