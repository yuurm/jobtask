package com.urmatskyyuri.gibdd.utils;

import org.apache.commons.lang3.ArrayUtils;
import java.util.Random;

public class LetterUtil {

    private static Random random = new Random();

    private static Character[] lettersList = {'А', 'Е', 'Т', 'О', 'Р', 'Н', 'У', 'К', 'Х', 'С', 'В', 'М'};

    public static Character next (Character character){
        return lettersList[(ArrayUtils.indexOf(lettersList, character + 1) % lettersList.length)];

    }

    public static Character random(){
        return lettersList[random.nextInt(lettersList.length)];
    }

}
