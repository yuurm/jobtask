package com.urmatskyyuri.gibdd.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class CarsNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    Character first, second, third;
    Integer number;

    @Override
    public String toString() {
        String result = "";

        result = result + first;
        if(number < 100){
            if(number < 10){
                result = result + "00" + number;
            } else {
                result = result + "0" + number;
            }
        } else {
            result = result + number;
        }

        result = result + second + third + "116RUS";

        return result;
    }
}
