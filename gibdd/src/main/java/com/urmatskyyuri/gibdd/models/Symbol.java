package com.urmatskyyuri.gibdd.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Random;

import static java.util.Arrays.asList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Symbol {

    private static Character[] lettersList =
            {'А', 'Е', 'Т', 'О', 'Р', 'Н', 'У', 'К', 'Х', 'С', 'В', 'М'};
    private Character letter;

    public boolean next() {
        Character temp = letter;
        this.letter = lettersList[(Arrays.asList(lettersList).indexOf(letter) + 1) % lettersList.length];
        return letter > temp;

    }

    public static Symbol random(){
        Random random = new Random();
        return Symbol.builder()
                     .letter(lettersList[random.nextInt(lettersList.length)])
                     .build();
    }
}
