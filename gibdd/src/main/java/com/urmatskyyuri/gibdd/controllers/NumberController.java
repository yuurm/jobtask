package com.urmatskyyuri.gibdd.controllers;


import com.urmatskyyuri.gibdd.models.CarsNumber;
import com.urmatskyyuri.gibdd.services.NumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class NumberController {

    @Autowired
    private NumberService numberService;

    @Autowired
    private CarsNumber carsNumber;

    @GetMapping("/next")
    @ResponseBody
    String nextNumber(){
        carsNumber = numberService.nextNumber(carsNumber);
        return carsNumber.toString();
    }

    @GetMapping("/random")
    @ResponseBody
    String randomNumber(){
        carsNumber = numberService.randomNumber();
        return carsNumber.toString();
    }


}
