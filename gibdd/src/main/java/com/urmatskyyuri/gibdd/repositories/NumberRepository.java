package com.urmatskyyuri.gibdd.repositories;

import com.urmatskyyuri.gibdd.models.CarsNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NumberRepository extends JpaRepository<CarsNumber, Long> {
}
